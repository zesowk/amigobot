package fr.zesowk.amigobot;

import java.util.Scanner;

import javax.security.auth.login.LoginException;

import fr.zesowk.amigobot.command.CommandMap;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.exceptions.RateLimitedException;

public class AmigoBot implements Runnable{
	
	/*
	 * AmigoBot par ZeSowK
	 * 
	 */
	
	private final JDA jda;
	private final CommandMap commandMap = new CommandMap(this);
	private final Scanner scanner = new Scanner(System.in);
	private final String token = ""; // Entrez votre token ici
	
	
	private boolean running;
	
	public AmigoBot() throws LoginException, IllegalArgumentException, RateLimitedException {
		jda = new JDABuilder(AccountType.BOT).setToken(token).setGame(Game.of("=help")).buildAsync();
		jda.addEventListener(new BotListener(commandMap));
		System.out.println("AmigoBot connected !");
		
	}
	
	public JDA getJda() {
		return jda;
	}
	
	public void setRunning(boolean running) {
		this.running = running;
	}

	@Override
	public void run() {
		running = true;
		
		while (running) {
			if(scanner.hasNextLine()) commandMap.commandConsole(scanner.nextLine());
		}
		
		scanner.close();
		System.out.println("AmigoBot Stopped");
		jda.shutdown();
		System.exit(0);
	}
	
	public static void main(String[] args){
		try {
			AmigoBot amigoBot = new AmigoBot();
			new Thread(amigoBot, "bot").start();
		} catch (LoginException | IllegalArgumentException | RateLimitedException e) { e.printStackTrace(); }
	}
	
	
}
