package fr.zesowk.amigobot;

import fr.zesowk.amigobot.command.CommandMap;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.EventListener;

public class BotListener implements EventListener{
	
	private final CommandMap commandMap;
	
	public BotListener(CommandMap commandMap){
		this.commandMap = commandMap;
	}
	
	@Override
	public void onEvent(Event event){
		//GuildMemberJoinEvent
		if(event instanceof MessageReceivedEvent) onMessage((MessageReceivedEvent) event);
		else if(event instanceof GuildMemberJoinEvent) onJoin((GuildMemberJoinEvent) event);
	}
	
	private void onMessage(MessageReceivedEvent event){
		if(event.getAuthor().equals(event.getJDA().getSelfUser())) return;
		
		String message = event.getMessage().getContent();
		if(message.startsWith(commandMap.getTag())){
			message = message.replaceFirst(commandMap.getTag(), "");
			if(commandMap.commandUser(event.getAuthor(), message, event.getMessage())){
				return;
			}
		}
	}

	private void onJoin(GuildMemberJoinEvent event){
		User user = event.getUser();
		this.sendPrivateMessage(user, ":mega::rotating_light:  Bienvenue � toi sur le Discord Los Amigos. Tu n'as acc�s � rien. Il faut que tu demande � un Administrateur pour te donner les permissions, car ce Discord est priv� !");
		event.getGuild().getTextChannelById("359007109008064513").sendMessage(":rotating_light: @ZeSowK#8250 Le joueur "+ event.getUser().getName() +" est en attente d'approbation !").queue();
	}
	private void sendPrivateMessage(User user, String content) {
	        // openPrivateChannel provides a RestAction<PrivateChannel>
	        // which means it supplies you with the resulting channel
	    user.openPrivateChannel().queue((channel) ->
	    {
	       channel.sendMessage(content).queue();
	    });
	}
}
