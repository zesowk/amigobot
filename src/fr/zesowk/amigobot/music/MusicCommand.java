package fr.zesowk.amigobot.music;

import fr.zesowk.amigobot.command.Command;
import fr.zesowk.amigobot.command.Command.ExecutorType;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.entities.VoiceChannel;

public class MusicCommand {

	private final MusicManager manager = new MusicManager();
	private boolean muted = false;
	@Command(name="play",description="Cette commande permet de lancer des musiques youtubes (=play [url youtube])." ,type=ExecutorType.USER,power=150)
	private void play(Guild guild, TextChannel textChannel, User user, String command){
		
		if(guild == null) return;
		
		
		if(!guild.getAudioManager().isConnected() && !guild.getAudioManager().isAttemptingToConnect()){
			VoiceChannel voiceChannel = guild.getMember(user).getVoiceState().getChannel();
			if(voiceChannel == null){
				textChannel.sendMessage("Vous devez �tre connect� � un salon vocal.").queue();
				return;
			}
			guild.getAudioManager().openAudioConnection(voiceChannel);
			guild.getAudioManager().setSelfDeafened(true);
		}
		
		manager.loadTrack(textChannel, command.replaceFirst("play ", ""));
	}
	
	@Command(name="skip",description="Cette commande permet de passer � la musique suivante.",type=ExecutorType.USER,power=150)
	private void skip(Guild guild, TextChannel textChannel){
		if(!guild.getAudioManager().isConnected() && !guild.getAudioManager().isAttemptingToConnect()){
			textChannel.sendMessage("Le player n'as pas de piste en cours.").queue();
			return;
		}
		
		manager.getPlayer(guild).skipTrack();
		textChannel.sendMessage("La lecture est pass� � la piste suivante.").queue();
	}
	
	@Command(name="clear",description="Cette commande permet de nettoyer la liste d'attente des musiques.",type=ExecutorType.USER,power=150)
	private void clear(TextChannel textChannel){
		MusicPlayer player = manager.getPlayer(textChannel.getGuild());
		
		if(player.getListener().getTracks().isEmpty()){
			textChannel.sendMessage("Il n'y a pas de piste dans la liste d'attente.").queue();
			return;
		}
		
		player.getListener().getTracks().clear();
		textChannel.sendMessage("La liste d'attente � �t� vid�.").queue();
	}
	
	@Command(name="queue",description="Affiche des musiques dans la musique d'attente.",type=ExecutorType.USER)
	private void queue(TextChannel textChannel) throws InterruptedException{
		MusicPlayer player = manager.getPlayer(textChannel.getGuild());
		if(player.getListener().getTracks().isEmpty()){
			textChannel.sendMessage("Il n'y a aucune musique dans la liste d'attente").queue();
			return;
		}
		
		textChannel.sendMessage("Voici les musiques dans la liste d'attente:").queue();
		int i;
		for(i = 0; i < player.getListener().getTracks().size(); i++){
			textChannel.sendMessage("**"+i+"**"+ ": `"+ player.getListener().getTracks().element().getInfo().title +"`").queue();
		}
		
	}
	
	@Command(name="mute",description="Permet de mute le bot.",type=ExecutorType.USER)
	private void mute(TextChannel textChannel, Guild guild) throws InterruptedException{
		if(muted = true){
			guild.getAudioManager().setSelfMuted(false);
			muted = false;
			return;
		}
		guild.getAudioManager().setSelfMuted(true);
		muted = true;
	}
}
