package fr.zesowk.amigobot.command.defaut;

import java.awt.Color;

import fr.zesowk.amigobot.AmigoBot;
import fr.zesowk.amigobot.command.Command;
import fr.zesowk.amigobot.command.Command.ExecutorType;
import fr.zesowk.amigobot.command.CommandMap;
import fr.zesowk.amigobot.command.SimpleCommand;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;

public class CommandDefault {
	
	private final AmigoBot amigoBot;
	private final CommandMap commandMap;
	
	public CommandDefault(AmigoBot amigoBot, CommandMap commandMap) {
		this.amigoBot = amigoBot;
		this.commandMap = commandMap;
	}
	
	@Command(name="stop",type=ExecutorType.CONSOLE)
	private void stop(){
		amigoBot.setRunning(false);
	}
	
	@Command(name="help",description="Commande pour trouver les autres commandes",type=ExecutorType.USER)
	private void help(User user, MessageChannel channel, Guild guild){
		EmbedBuilder builder = new EmbedBuilder();
		builder.setAuthor("Liste des Commandes", null, null);
		builder.setTitle(":rotating_light: Ceci est une liste des commandes ! ");
		
		for(SimpleCommand command : commandMap.getCommands()){
			if(command.getExecutorType() == ExecutorType.CONSOLE) continue;
			builder.addField(commandMap.getTag() + command.getName(), command.getDesciption(), false);
		}
		builder.setThumbnail("https://image.noelshack.com/fichiers/2017/37/7/1505634779-information-1481584-640-1.png");
		builder.setColor(Color.white);
		
		channel.sendMessage(builder.build()).queue();
	}
	
	@Command(name="info",description="Pour avoir des informations sur soi-m�me.", type=ExecutorType.USER)
	private void info(User user, MessageChannel channel){
		if(channel instanceof TextChannel){
			TextChannel textChannel = (TextChannel)channel;
			if(!textChannel.getGuild().getSelfMember().hasPermission(Permission.MESSAGE_EMBED_LINKS)) return;
		}
		
		EmbedBuilder builder = new EmbedBuilder();
		builder.setAuthor(user.getName(), null, user.getAvatarUrl()+"?size=256");
		builder.setTitle("Informations sur "+ user.getName());
		builder.setDescription("[>](1)Vous �tes "+ user.getName() +".\r[>](2)Vous vous �tes inscrit sur Discord le "+ user.getCreationTime() +"\r[>](3)Votre id Discord est le " + user.getId());
		builder.setFooter("Ces informations peuvent vous �tre utiles en cas de probl�mes.", null);
		builder.setColor(Color.white);
		
		channel.sendMessage(builder.build()).queue();
	}
}
