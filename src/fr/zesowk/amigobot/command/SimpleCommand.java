package fr.zesowk.amigobot.command;

import java.lang.reflect.Method;

import fr.zesowk.amigobot.command.Command.ExecutorType;

public final class SimpleCommand {
	
	private final String name, desciption;
	private final ExecutorType executorType;
	private final Object object;
	private final Method method;
	private final int power;
	
	public SimpleCommand(String name, String desciption, ExecutorType executorType, Object object, Method method, int power) {
		super();
		this.name = name;
		this.desciption = desciption;
		this.executorType = executorType;
		this.object = object;
		this.method = method;
		this.power = power;
	}

	public String getName() {
		return name;
	}

	public String getDesciption() {
		return desciption;
	}

	public ExecutorType getExecutorType() {
		return executorType;
	}

	public Object getObject() {
		return object;
	}

	public Method getMethod() {
		return method;
	}
	
	public int getPower(){
		return power;
	}
}
